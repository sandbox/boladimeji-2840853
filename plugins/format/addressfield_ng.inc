<?php

/**
 * @file
 * Address field plugin providing a detailed form for Nigeria addresses.
 */

$plugin = array(
  'title' => t('Address form (specific for Nigeria)'),
  'format callback' => 'addressfield_ng_format_address_generate',
  'type' => 'address',
  'weight' => 100,
);

/**
 * Format callback for Nigeria address.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_ng_format_address_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'NG') {
    if ($context['mode'] == 'form') {
      $format['street_block']['thoroughfare']['#access'] = FALSE;
      $format['street_block']['premise']['#access'] = FALSE;

      $format['street_block']['thoroughfare_number'] = array(
      		'#type' => 'textfield',
      		'#title' => t('No'),
      		'#required' => TRUE,
      		'#attributes' => array('class' => array('number')),
      		'#size' => 4,
      		'#default_value' => isset($address['thoroughfare_number']) ? $address['thoroughfare_number'] : '',
      );
      
      $format['street_block']['thoroughfare_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Street'),
        '#required' => TRUE,
        '#attributes' => array('class' => array('street')),
        '#size' => 30,
        '#default_value' => isset($address['thoroughfare_name']) ? $address['thoroughfare_name'] : '',
      );
      
      $format['street_block']['premise_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Bus Stop'),
        '#required' => FALSE,
        '#attributes' => array('class' => array('premise-name')),
        '#size' => 30,
        '#default_value' => isset($address['premise_name']) ? $address['premise_name'] : '',
      );

      
      // List of states in Nigeria
      // Select from the option 
      $format['locality_block']['administrative_area']['#weight'] = 12;
      $format['locality_block']['administrative_area']['#required'] = TRUE;
      $format['locality_block']['administrative_area']['#options'] = array(
      		''   => t('--Choose a state--'),
      		'Abia-State' => t('Abia'),
      		'Adamawa-State' => t('Adamawa'),
      		'Akwa Ibom-State' => t('Akwa Ibom'),
      		'Anambra-State' => t('Anambra'),
      		'Bauchi-State' => t('Bauchi'),
      		'Bayelsa-State' => t('Bayelsa'),
      		'Benue-State' => t('Benue'),
      		'Borno-State' => t('Borno'),
      		'Cross River-State' => t('Cross River'),
      		'Delta-State' => t('Delta'),
      		'Ebonyi-State' => t('Ebonyi'),
      		'Edo-State' => t('Edo'),
      		'Ekiti-State' => t('Ekiti'),
      		'Enugu-State' => t('Enugu'),
      		'Abuja' => t('Federal Capital Territory'),
      		'Gombe-State' => t('Gombe'),
      		'Imo-State' => t('Imo'),
      		'Jigawa-State' => t('Jigawa'),
      		'Kaduna-State' => t('Kaduna'),
      		'Kano-State' => t('Kano'),
      		'Katsina-State' => t('Katsina'),
      		'Kebbi-State' => t('Kebbi'),
      		'Kogi-State' => t('Kogi'),
      		'Kwara-State' => t('Kwara'),
      		'Lagos-State' => t('Lagos'),
      		'Nassarawa-State' => t('Nassarawa'),
      		'Niger-State' => t('Niger'),
      		'Ogun-State' => t('Ogun'),
      		'Ondo-State' => t('Ondo'),
      		'Osun-State' => t('Osun'),
      		'Oyo-State' => t('Oyo'),
      		'Plateau-State' => t('Plateau'),
      		'Rivers-State' => t('Rivers'),
      		'Sokoto-State' => t('Sokoto'),
      		'Taraba-State' => t('Taraba'),
      		'Yobe-State' => t('Yobe'),
      		'Zamfara-State' => t('Zamfara'),
      );
      
      $format['locality_block']['locality'] = array(
      		'#title' => t('Area/Town'),
      		'#size' => 30,
      		'#required' => TRUE,
      		'#prefix' => ' ',
      		'#attributes' => array('class' => array('locality')),
      		'#weight' => 1,
      );
      
      // Default district for Lagos state.
    /*  if ($address['administrative_area'] == 'Lagos') {
      	$format['locality_block']['locality']['#options'] = array(
      			'' => t('-Choose an area-'),
      			'Agbado' => t('Agbado'),
      			'Agboyi' => t('Agboyi'),
      			'Agege' => t('Agege'),
      			'Ajeromi' => t('Ajeromi'),
      			'Alimosho' => t('Alimosho'),
      			'Apapa' => t('Apapa'),
      			'Apapa-Iganmu' => t('Apapa-Iganmu'),
      			'Badagry West' => t('Badagry West'),
      			'Badagry' => t('Badagry'),
      			'Bariga' => t('Bariga'),
      			'Bayeku' => t('Bayeku'),
      			'Coker Aguda' => t('Coker Aguda'),
      			'Egbe Idimu' => t('Egbe Idimu'),
      			'Ejigbo' => t('Ejigbo'),
      			'Epe' => t('Epe'),
      			'Eti Osa East' => t('Eti Osa East'),
      			'Eti Osa West' => t('Eti Osa West'),
      			'Festac Town' => t('Festac Town'),
      			'Iba' => t('Iba'),
      			'Ibeju' => t('Ibeju'),
      			'Ifako-Ijaiye' => t('Ifako-Ijaiye'),
      			'Ifelodun' => t('Ifelodun'),
      			'Igando' => t('Igando'),
      			'Igbogbo' => t('Igbogbo'),
      			'Ijede' => t('Ijede'),
      			'Ikeja' => t('Ikeja'),
      			'Ikorodu' => t('Ikorodu'),
      			'Ikorodu North' => t('Ikorodu North'),
      			'Ikorodu West' => t('Ikorodu West'),
      			'Ikosi Ejinrin' => t('Ikosi Ejinrin'),
      			'Ikotun' => t('Ikotun'),
      			'Ikoyi' => t('Ikoyi'),
      			'Imota' => t('Imota'),
      			'Ipaja' => t('Ipaja'),
      			'Iru' => t('Iru'),
      			'Isolo' => t('Isolo'),
      			'Itire Ikate' => t('Itire Ikate'),
      			'Ketu' => t('Ketu'),
      			'Kosofe' => t('Kosofe'),
      			'Lagos Island East' => t('Lagos Island East'),
      			'Lagos Island West' => t('Lagos Island West'),
      			'Lagos Mainland' => t('Lagos Mainland'),
      			'Lekki' => t('Lekki'),
      			'Mosan' => t('Mosan'),
      			'Mushin' => t('Mushin'),
      			'Odi Olowo' => t('Odi Olowo'),
      			'Ojo' => t('Ojo'),
      			'Ojodu' => t('Ojodu'),
      			'Ojokoro' => t('Ojokoro'),
      			'Ojuwoye' => t('Ojuwoye'),
      			'Oke-Odo' => t('Oke-Odo'),
      			'Okunola' => t('Okunola'),
      			'Olorunda' => t('Olorunda'),
      			'Onigbongbo' => t('Onigbongbo'),
      			'Oriade' => t('Oriade'),
      			'Orile Agege' => t('Orile Agege'),
      			'Oshodi' => t('Oshodi'),
      			'Oto-Awori' => t('Oto-Awori'),
      			'Shomolu' => t('Shomolu'),
      			'Surulere' => t('Surulere'),
      			'Victoria Island' => t('Victoria Island'),
      			'Yaba' => t('Yaba'),
      	);
      }
      
      // Default district for FCT (Abuja).
      if ($address['administrative_area'] == 'Abuja') {
      	$format['locality_block']['locality']['#options'] = array(
      			'' => t('-Choose an area-'),
      			'Asokoro' => t('Asokoro'),
      			'Central Business District' => t('Central Business District'),
      			'Garki Area 1' => t('Garki Area 1'),
      			'Garki Area 2' => t('Garki Area 2'),
      			'Garki Area 3' => t('Garki Area 3'),
      			'Garki Area 7' => t('Garki Area 7'),
      			'Garki Area 8' => t('Garki Area 8'),
      			'Garki Area 10' => t('Garki Area 10'),
      			'Garki Area 11' => t('Garki Area 11'),
      			'Maitama' => t('Maitama'),
      			'Wuse II' => t('Wuse II'),
      			'Wuse Zone 1' => t('Wuse Zone 1'),
      			'Wuse Zone 2' => t('Wuse Zone 2'),
      			'Wuse Zone 3' => t('Wuse Zone 3'),
      			'Wuse Zone 4' => t('Wuse Zone 4'),
      			'Wuse Zone 5' => t('Wuse Zone 5'),
      			'Wuse Zone 6' => t('Wuse Zone 6'),
      			'Wuse Zone 7' => t('Wuse Zone 7'),
      			'Three Arms Zone' => t('Three Arms Zone'),
      	);
      }
      
      // Default district for FCT (Abuja).
      if ($address['administrative_area'] == 'Rivers') {
      	$format['locality_block']['locality']['#options'] = array(
      			'' => t('-Choose an area-'),
      			'Alo-mini' => t('Alo-mini'),
      			'Amadi flat' => t('Amadi flat'),
      			'Bori kiri' => t('Bori kiri'),
      			'Diobu mile 1' => t('Diobu mile 1'),
      			'Diobu mile 2' => t('Diobu mile 2'),
      			'Diobu mile 3' => t('Diobu mile 3'),
      			'Diobu mile 4' => t('Diobu mile 4'),
      			'D-line' => t('D-line'),
      			'Eagle island rumueme/Oroakwo' => t('Eagle island rumueme/Oroakwo'),
      			'Igwa layout' => t('Igwa layout'),
      			'Ikwerre' => t('Ikwerre'),
      			'Magbuoba' => t('Magbuoba'),
      			'Mbiama Rd.' => t('Mbiama Rd.'),
      			'New G.R.A' => t('New G.R.A'),
      			'New layout' => t('New layout'),
      			'Ochoma layout' => t('Ochoma layout'),
      			'Old G.R.A' => t('Old G.R.A'),
      			'Old Township' => t('Old Township'),
      			'Port harcourt Rd.' => t('Port harcourt Rd.'),
      			'Rumukrueshi' => t('Rumukrueshi'),
      			'Rumuokwuta' => t('Rumuokwuta'),
      			'Town' => t('Town'),
      			'Trans  Amadi' => t('Trans  Amadi'),
      	);
      } */                         
    }
  }
}
