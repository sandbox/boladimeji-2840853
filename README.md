# Addressfield Nigeria

This module is a plugin for [Addressfield][0] that provides individual fields
for various address components.

When enabled, the address form will collect

* Street name
* Street number
* Block / Building
* Floor
* Apartment / Office
* County

The regular _Address 1_ and _Address 2_ fields will be automatically filled
based on the other values.

## Installation

Just download and enabled the module. 

## Usage

Enable _Address form (specific for Nigeria)_ in the field settings.

